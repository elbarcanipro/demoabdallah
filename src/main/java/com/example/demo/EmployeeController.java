package com.example.demo;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/employees")
public class EmployeeController {
    private final GetAllEmployees getAllEmployees;

    @RequestMapping("/all")
    public List<Employee> getAllEmployees() {
        return getAllEmployees.execute();
    }
}
