package com.example.demo;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class GetAllEmployees {

    private final EmployeeRepository employeeRepository;

    public List<Employee> execute() {
        return employeeRepository.findAll();
    }
}
